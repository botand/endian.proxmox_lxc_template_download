# proxmox_lxc_template_download

Downloads a specified LXC template if not already available.

## Requirements

Run on a Proxmox node with `pveam` installed.

## Role Variables

| Name         | Default |                                  Description |
| :----------- | ------- | -------------------------------------------: |
| `os_name`    |         |                  Name of the OS to download. |
| `storage`    | `local` | Storage location where to store the template |
| `exact_name` |         |     Exact name of a LXC template to download |

## Dependencies

None.

## Example Playbook

```yaml
---
host: pve_node
tasks:
  - name: Include role
    include_role: proxmox_lxc_tempalte_download
    vars:
      os_name: archlinux-base
      storage: local
```

## License

GPLv3

## Author Information

Andreas Botzer
